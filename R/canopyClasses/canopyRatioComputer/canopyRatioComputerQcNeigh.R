



require(R6)



canopyRatioComputerQcNeigh <- R6Class(
  
  
  #
  #Name
  #
  "canopyRatioComputerQcNeigh",
  
  #--
  #Public
  #---
  
  
  public = list(
    
    #' Constructor
    #'
    initialize = function( socioDemoShp,
                           canopyPathFallback = here("Data","GeoData","CanopyQc", "vdq-canopee.shp", "outshp") ,
                           shpWritePath =here("Data","GeoData","CanopyQc", "vdq-canopee.shp", "outshp") , # shpWritePath should be the same as canopyPathFallback
                           csvWritePath = here("Data", "Csv", "canopy", "qcCityTreeRatio.csv"),
                           useManualComputation = F  ){
      
      private$socioDemoShp <- socioDemoShp
      private$shp <- socioDemoShp$getShp()
      
      #Watch out, for qc, we need to add the neighbourhood
      private$canopyPathFallback <- canopyPathFallback
      private$shpWritePath <- shpWritePath
      private$csvWritePath <- csvWritePath
 
      
      private$proj <- private$socioDemoShp$getProj()
      private$useManualComputation <- useManualComputation
    },
    
    
    
    
    computeCanopyRatio = function(){
      
      if(private$useManualComputation == T){
        print("Using automatic/default (slower) computation")
        
        private$canopyRatioComputerDefault <- canopyRatioComputerDefault$new(private$socioDemoShp,
                                                                             private$canopyPathFallback ,
                                                                             private$shpWritePath,
                                                                             private$csvWritePath)
        
        private$shp <- private$canopyRatioComputerDefault$computeCanopyRatio()
        
      }else{
        print(glue("Automatic-(using precomputed values) computation "))
        private$computeAutomaticCanopyRatio()
      }
      
      return(private$shp)
    },
    
    getProj=function(){
      return(private$proj)
    },
   
    getShp=function(){
      return(private$shp)
    }
  
  ),
  
  
  
  private = list( 
    socioDemoShp=NULL,
    shp=NULL,
    #
    canopyPathFallback=NULL,
    shpWritePath=NULL,
    csvWritePath=NULL,
    #
    proj=NULL,
    #
    useManualComputation=NULL,
    #
    canopyRatioComputerDefault=NULL,
 
    
    
        
    #' Special fct that only works for Qc city neigh canopy BY NEIGHBOURHOOD
    #' 
    #' The city alreay split the canopy shp files by neigh + computed the area of each polygon
    #' (manual and auto computation of areas match the results of st_area with Lambert 32198 proj)
    #'
    #' The neigh area has also alreay been computed so we can use itz
    #'
    #' Useful to speed up the computation since the area has already been computed by the city
    #'
    #' @return
    #' @export
    #'
    #' @examples
    computeAutomaticCanopyRatio = function(){
      
      warn("Computing the automatic canopy ratio for qc: ignoring the max time param")
      
      private$shp$ratioTrees <- rep(NA, nrow(private$shp))
      listSubsetErr=list()
      
      for(k in 1:nrow(private$shp)){
        
       tryCatch({
          
          #Read the canopy for that specific neighbourhood
          dirName <- private$shp$origName[[k]]
          dirQc <- private$canopyPathFallback
          shpCanopyQcNeigh <- st_read( file.path(dirQc, dirName)) 
          
          private$shp$ratioTrees[[k]] <- sum(shpCanopyQcNeigh$SHAPE_Area, na.rm = T) / private$shp$SUPERFICIE[[k]]  
        },
        error=function(e){
          print(glue("Fatal error in automatic qc ratio computation at {private$shp$NOM[[k]]} - {conditionMessage(e)}"))
          listSubsetErr <- rlist::list.append(listSubsetErr, private$shp$NOM[[k]] )
        }
        )
        
      }
      
      #Quick warning
      if(length(listSubsetErr) > 0){
        listErrStr <- paste(listSubsetErr, collapse = ",",sep=",")
        print(glue("Warning! The following features have errors: {listErrStr}"))
      }
      
      #Write to csv
      private$writeShpToCsvAutomatic()
      
      return( private$shp )
      
    },
    
    
    
    writeShpToCsvAutomatic=function(){
      
      #Remove the geometry
      df <- private$shp %>% st_set_geometry(NULL)
      
      #Overwrite
      write_csv(df, 
                path = private$csvWritePath , 
                append = F)
      
    }
      

  )
  
)





