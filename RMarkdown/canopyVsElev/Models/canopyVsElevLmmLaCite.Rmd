---
title: "Untitled"
author: "Charles"
date: "April 21, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
library(miceadds)
library(glue)
library(sf)
 
library(lme4)
library(lmerTest)
library(mgcv)

library(ggplot2)
library(patchwork)
library(ggmosaic)
library(ggspatial)

library(raster)
library(rgeos)
library(sf)

```

#Parameters
```{r}

options(na.action = "na.exclude")

set.seed(123)

useQc <- T

useLogPovertyForClustering <- T
povertyStr <- ifelse(useLogPovertyForClustering, "logPoverty", "logPovertyScaled" )

csd <- ifelse(useQc,  "2423027",  "2466023")
city <- ifelse(useQc, "Quebec", "Montreal")

Census2SfSp:::setCancensusOptions()


  if(useQc){
    nS <- neighShp::neighShpQc$new( neighPath = here("Data","GeoData", "QcNeighbourhoods") )
  }else{
    nS <- neighShp::neighShpMtl$new(  )
  }

idVar <-  nS$getID()
  
useHighlight <- F #To highlight the medium cluster 

```


---
---
 
#Socio demo prep

 
```{r}

    #Ratio trees df by city 
    resultsList <- getNeighDonCsv(useQc)
    listNeighDone <- resultsList[["listNeighDone"]]
    listNeighDoneCsv <- resultsList[["listNeighDoneCsv"]]

  
    ##Get the df where rows correspond to DAs and that has columns ratioTrees
    dfCity <- getCityDfRatioTrees(useQc)

```


#Get the elevation rasters
```{r}

#Get the raster 
basePath <- "/home/charles/Projects/neighRaster/Data/GeoData"
qcRastInst <- neighRaster::neighRasterQcFactory$new(rasterPath = file.path(basePath, "/Raster/cdem_dem_021L.tif"),
                                                    neighPath = file.path(basePath, "/QuebecNeighbourhoods")
)

 
qcNeighRast <- qcRastInst$createNeighRaster()
qcRast <- qcNeighRast$getRaster()

```

#Water shp
```{r}

#use a buffer to avoid filtering out rivers like the Jacques-Cartier that are close, but do not intersect the city limits
nSWaterQc <- water::hydroShpQc$new(riversPath = "/home/charles/Projects/Water/Data/GeoData/Hydro/hydro_l",
                           lakesPath = "/home/charles/Projects/Water/Data/GeoData/Hydro/hydro_s",
                           neighPath = "/home/charles/Projects/Vegetation/Data/GeoData/QcNeighbourhoods",
                           buff = 0.1
                            )  

```

#Data prep 
```{r}
  
  listResults <- dataPrepElevCanopy(dfCity, csd, nS, qcRastInst, nSWaterQc)
  
  shpDAUnique <- listResults$shpDAUnique
  shpNeigh <- listResults$shpNeigh
  shpAllWaterLines <- listResults$shpAllWaterLines
  
```




#Filter La Cité
```{r}

shpDAV2 <- st_join(shpDAUnique %>% dplyr::select(-c(Arrondissement, neigh)),
        shpNeigh %>% dplyr::select(NOM), 
        largest=T)
 
shpDAV2 %<>% addBoroughQc(lhsID="NOM")

shpDAUniqueLaCite <-   shpDAV2 %>% filter(Arrondissement %in% c("La Cité", "Limoilou" ) )  
 
shpNeighLaCite <-shpNeigh %>% filter(Arrondissement %in% c("La Cité", "Limoilou" ) )  

ggplot(shpDAUniqueLaCite) + geom_sf()

``` 
 

#Add the the binary indicator for DA crossing cliff based on slope from elev raster
```{r}

shpDAUniqueLaCite <- addCliffPresence(shpDAUniqueLaCite, qcRast)

shpDAUniqueLaCite <- addDistanceToCliff(shpDAUniqueLaCite, qcRast)

```


---
---

#Quick visual validation

##Maps the 2***2 possible interactions for binary cliff and water
```{r}

shpDAUniqueLaCite %<>% mutate(touchClass = factor(touchesWater*2**0 + touchesCliff*2**1 ))


ggplot(shpDAUniqueLaCite) + 
  geom_sf(aes(fill=touchClass)) + 
  labs(caption="Codes: 0 - touhes nothing\n1 - touches water only\n2 - touches cliff only\n3 - touches cliff AND water") + 
  scale_fill_brewer()


shpPlot <- shpDAUniqueLaCite %>% 
  filter_at(vars("touchesWater","touchesCliff","weightedRatioTrees"), all_vars(!is.na(.)))   

 
```

##Distance from cliff and water
```{r}

pCliff <- ggplot(shpDAUniqueLaCite %>% mutate(`Distance to cliff`=distanceToCliff)) + 
  geom_sf(aes(fill=`Distance to cliff`), lwd=0) + 
  scale_fill_continuous() + 
  theme_bw() 

pWater <- ggplot(shpDAUniqueLaCite %>% mutate(`Distance to water`=distanceToWater)) + 
  geom_sf(aes(fill=`Distance to water`), lwd=0) + 
  scale_fill_continuous()+ 
  theme_bw()+
  xlab("") + ylab("") + 
  annotation_scale(location="br",width_hint=0.3 ) + 
  annotation_north_arrow(location="tl",
                         style = north_arrow_fancy_orienteering,
                         height = unit(0.5,"in")) 


pBoth <- (pCliff | pWater ) + plot_annotation(title="Distance for different geographic indicator",
                                 subtitle = "La Cité-Limoilou borough, Quebec City",
                                 caption = "Distance computed from each dissemination area centroid")

(pBoth)

ggsave(here("Figures","canopyVsElev","distanceMaps.png"),
       pBoth,
       width=15,
       height=7)
```


##Mosaic
```{r}


pMosaic <- ggplot( shpPlot ) + 
  geom_mosaic( aes(  x=product(touchesWater), fill=ratioTreeQuantClass  ) ) +
  facet_wrap(~ touchesCliff)  + 
  scale_fill_brewer()+
  labs(x = "DA touches water", 
       y = "Proportion",
       title='Canopy profiles as a fonction of cliff and water presence',
       subtitle = "La Cité borough, Quebec City")   
(pMosaic)


```



```{r}

#Not enough data for full interaction water*cliff*neigh

dfCount <- shpPlot %>%
  st_set_geometry(NULL) %>% 
  count(touchesWater, touchesCliff, NOM )


ggplot(dfCount) + 
  geom_tile(aes(x=touchesWater,y=touchesCliff,fill=n)) + 
  facet_wrap(~NOM)


dfByNeigh <- shpPlot %>%
  st_set_geometry(NULL) %>% 
  group_by(touchesWater, touchesCliff, NOM ) %>% 
  summarise(weightedRatioTrees=mean(weightedRatioTrees))

ggplot(dfByNeigh) + 
  geom_tile(aes(x=touchesWater,y=touchesCliff,fill=weightedRatioTrees)) + 
  facet_wrap(~NOM)

```
 
 
##Grid
```{r}

ggplot(shpDAUniqueLaCite) + 
  geom_point(aes(x=distanceToWater, y=weightedRatioTrees))+  
  facet_wrap(~NOM)

```

```{r}

ggplot(shpDAUniqueLaCite) + 
  geom_point(aes(x=distanceToCliff, y=weightedRatioTrees))+  
  facet_wrap(~NOM)

```
 
 
---
---



#Models

##Transform the response to a non-negative rv
```{r}

shpDAUniqueLaCiteNoNa %<>% mutate(logRatioPercentTrees = log(weightedRatioTrees*100) )
assertthat::are_equal( min(shpDAUniqueLaCiteNoNa$logRatioPercentTrees) >= 0, T )

```

##Add in treeArea which could be used for count data (~counting number of square meters in polygon with trees)
```{r}

shpDAUniqueLaCiteNoNa %<>% mutate(totalArea =  units::set_units(st_area(shpDAUniqueLaCiteNoNa), m^2) %>% as.numeric)
shpDAUniqueLaCiteNoNa %<>% mutate(treeArea = weightedRatioTrees * totalArea)
 
 
ggplot(shpDAUniqueLaCiteNoNa) + geom_histogram(aes(x=treeArea))
                                              
```


---
---

#Binomial on ?proportion daa?

##Binomial + Fixed effect with neigh effect, but no interactions
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
lmFit1 <- glm(weightedRatioTrees ~ touchesWater  +touchesCliff  + NOM, 
              data= shpDAUniqueLaCite ,
              family = binomial(link="logit"))
lmFit1 %>% summary

sd(predict(lmFit1),na.rm=T) #variance of prediction is actually much LARGER than the var of the actual outcome variable
sd(shpDAUniqueLaCite$weightedRatioTrees,na.rm=T)

car::vif(lmFit1)
```



##Binomial + Fixed effect with interactions
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
lmFit2 <- glm(weightedRatioTrees ~ touchesWater:NOM+touchesCliff:NOM, 
              data= shpDAUniqueLaCite ,
              family = binomial(link="logit"))
lmFit2 %>% summary


```


##Binomial + Fixed effects with popDensity as main effect
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
lmFit3 <- glm(weightedRatioTrees ~ touchesWater  +touchesCliff  + NOM + popDensityPerHa, 
              data= shpDAUniqueLaCite ,
              family = binomial(link="logit"))
lmFit3 %>% summary


shpDAUniqueLaCite %<>% mutate( predLm3 = predict(lmFit3) )
shpDAUniqueLaCite %<>% mutate( errLm3 = predLm3 - weightedRatioTrees )

dfVar <- computeVariogram(shpDAUniqueLaCite  , "errLm3")
ggplot(dfVar) + geom_point(aes(x=dist,y=gamma,size=np))
ggplot(  )  + geom_sf(data=shpNeighLaCite) + geom_point(data=shpDAUniqueLaCite, aes(x=lng,y=lat,fill=errLm3),shape=21) + scale_fill_gradient2( )

```

---
---


#Poisson on ?number square m2 with trees
```{r}

 
#E(numTrees|X) = totalArea * exp( beta'X )

lmFitPoisson1 <- glm(treeArea ~ touchesWater +touchesCliff + NOM + offset(log(totalArea))   ,
              data= shpDAUniqueLaCiteNoNa ,
              family = poisson)

lmFitPoisson1 %>% summary

#On linear scale whithout the offset - vals can be negative
shpDAUniqueLaCiteNoNa %<>% mutate(predPoisson1 = predict(lmFitPoisson1, 
                                                         newdata = shpDAUniqueLaCiteNoNa %>% mutate(totalArea=1),
                                                         type="link") 
)

#On the response scale after taking exp and whithout the offset - values all positive, (but are not bounded above so interpreting as proprortions is dubious)
shpDAUniqueLaCiteNoNa %<>% mutate(predPoisson2 = predict(lmFitPoisson1, 
                                                         newdata = shpDAUniqueLaCiteNoNa %>% mutate(totalArea=1),
                                                         type="response") 
)                               
     
#On the response scale after taking exp AND multiplying by offset - should correspond to area of trees 
shpDAUniqueLaCiteNoNa %<>% mutate(predPoisson3 = predict(lmFitPoisson1, 
                                                         newdata = shpDAUniqueLaCiteNoNa %>% mutate(totalArea=1),
                                                         type="response")  * totalArea 
)      
           
#This is a legitimate proportion                  
shpDAUniqueLaCiteNoNa %<>% mutate(predPoisson4 = predPoisson3/ totalArea )      
   
 
ggplot(shpDAUniqueLaCiteNoNa, aes(x=predPoisson1,y=weightedRatioTrees)) + 
  geom_point()

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predPoisson2,y=weightedRatioTrees)) + 
  geom_point()

mn <- min( min(shpDAUniqueLaCiteNoNa$treeArea), min(shpDAUniqueLaCiteNoNa$predPoisson3))
mx <- max( max(shpDAUniqueLaCiteNoNa$treeArea), max(shpDAUniqueLaCiteNoNa$predPoisson3))
l <- c(mn,mx)

#This fit doesn't seem all that bad
ggplot(shpDAUniqueLaCiteNoNa, aes(x=predPoisson3,y=treeArea)) + 
  geom_point() + 
  lims(x=l, y=l )

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predPoisson4,y=weightedRatioTrees)) + 
  geom_point()

```


---
---


#Gamma 

```{r}

 

lmFitGamma1 <- glm(logRatioPercentTrees ~ touchesWater +touchesCliff + NOM   ,
              data= shpDAUniqueLaCiteNoNa ,
              family = Gamma)

lmFitGamma1 %>% summary

#On the response scale after taking exp AND multiplying by offset - should correspond to area of trees 
shpDAUniqueLaCiteNoNa %<>% mutate(predGamma1 = predict(lmFitGamma1,  
                                                         type="response")   
)      
shpDAUniqueLaCiteNoNa %<>% mutate(predGamma2 = exp(predGamma1)/100   )   

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predGamma1,y=logRatioPercentTrees)) + 
  geom_point()  

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predGamma2,y=weightedRatioTrees)) + 
  geom_point()  

#Assess Gamma fit
shpDAUniqueLaCiteNoNa <- shpDAUniqueLaCite %>% filter(!is.na(weightedRatioTrees))
est <- fitdistrplus::fitdist(shpDAUniqueLaCiteNoNa$weightedRatioTrees, distr = "gamma", method = "mle")$estimate

EnvStats::qqPlot(x=predict(lmFitGamma1) ,  
                 y=shpDAUniqueLaCiteNoNa$weightedRatioTrees, 
                 param.list = list(est), 
                 distribution="Gamma")

EnvStats::qqPlot(x=predict(lmFitGamma1) ,  
                 y=shpDAUniqueLaCiteNoNa$weightedRatioTrees, 
                 estimate.params = T, 
                 distribution="Gamma")

```

---
---

#Gaussian
```{r}

 

lmFitGauss1 <- glm(logRatioPercentTrees ~ touchesWater +touchesCliff + NOM   ,
              data= shpDAUniqueLaCiteNoNa ,
              family = gaussian)

lmFitGauss1 %>% plot

lmFitGauss1 %>% summary

#On the response scale after taking exp AND multiplying by offset - should correspond to area of trees 
shpDAUniqueLaCiteNoNa %<>% mutate(predGauss1 = predict(lmFitGauss1,  
                                                         type="response")   
)      
shpDAUniqueLaCiteNoNa %<>% mutate(predGauss2 = exp(predGauss1)/100   )   

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predGauss1,y=logRatioPercentTrees)) + 
  geom_point()  

ggplot(shpDAUniqueLaCiteNoNa, aes(x=predGauss2,y=weightedRatioTrees)) + 
  geom_point()  

```

##Gam  with s(popDensity) as main effect
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
gamFit1 <- gam(logRatioPercentTrees ~ touchesWater  +touchesCliff  + NOM + s(popDensityPerHa), 
               data= shpDAUniqueLaCiteNoNa )
gamFit1 %>% summary

plot(gamFit1)

shpDAUniqueLaCiteNoNa %<>% mutate( predGam1 = predict(gamFit1) )
shpDAUniqueLaCiteNoNa %<>% mutate( errLGam1 = predGam1 - logRatioPercentTrees )

dfVar <- computeVariogram(shpDAUniqueLaCiteNoNa  , "errLGam1")
ggplot(dfVar) + geom_point(aes(x=dist,y=gamma,size=np))
ggplot(  )  + geom_sf(data=shpNeighLaCite) + geom_point(data=shpDAUniqueLaCiteNoNa, aes(x=lng,y=lat,fill=errLGam1),shape=21) + scale_fill_gradient2( )

```




##Gam effects with s(popDensity) + s(distanceWater) + s(distNanceCliff) as main effect
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
gamFit2 <- gam(logRatioPercentTrees ~   s(distanceToWater) + s(distanceToCliff)   + NOM + s(popDensityPerHa), 
               data= shpDAUniqueLaCiteNoNa )
gamFit2 %>% summary

plot(gamFit2)

shpDAUniqueLaCiteNoNa %<>% mutate( predGam2 = predict(gamFit2) )
shpDAUniqueLaCiteNoNa %<>% mutate( errGam2 = predGam2 - logRatioPercentTrees )

dfVar <- computeVariogram(shpDAUniqueLaCiteNoNa  , "errGam2")
ggplot(dfVar) + geom_point(aes(x=dist,y=gamma,size=np))
ggplot(  )  + geom_sf(data=shpNeighLaCite) + geom_point(data=shpDAUniqueLaCiteNoNa, aes(x=lng,y=lat,fill=errGam2),shape=21) + scale_fill_gradient2( )

```

## Mixed model by neighourhood + only binary vars
```{r}

#Can Only used to main effects - not all interaction terms can be estimated with the data
mixedFit <- lmer(weightedRatioTrees ~ touchesWater+touchesCliff + (touchesWater+touchesCliff|NOM), data= shpDAUniqueLaCite )
mixedFit %>% summary

dotplot(ranef(mixedFit))

```

##Mixed model with continuous distances
```{r}

 
#Can Only used to main effects - not all interaction terms can be estimated with the data
mixedFit2 <- lmer(weightedRatioTrees ~ distanceToWater+distanceToCliff + (distanceToWater+distanceToCliff|NOM), data= shpDAUniqueLaCite )
mixedFit2 %>% summary

dotplot(ranef(mixedFit2))
```


