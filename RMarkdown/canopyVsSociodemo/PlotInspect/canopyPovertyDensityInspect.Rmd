---
title: "Untitled"
author: "Charles"
date: "November 25, 2019"
output: html_document
---
 

#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
library(miceadds)
library(glue)

library(geofacet)
library(sf)
 
library(lme4)
library(lmerTest)

library(ggplot2)
library(ggrepel)
library(gghighlight)
library(cowplot)
library(patchwork)
```

#Parameters
```{r}

options(na.action = "na.exclude")

set.seed(123)

useQc <- T

useLogPovertyForClustering <- T
povertyStr <- ifelse(useLogPovertyForClustering, "logPoverty", "logPovertyScaled" )

csd <- ifelse(useQc,  "2423027",  "2466023")
city <- ifelse(useQc, "Quebec", "Montreal")

Census2SfSp:::setCancensusOptions()


  if(useQc){
    nS <- neighShp::neighShpQc$new( neighPath = here("Data","GeoData", "QcNeighbourhoods") )
  }else{
    nS <- neighShp::neighShpMtl$new(  )
  }

idVar <-  nS$getID()
  
useHighlight <- F #To highlight the medium cluster 

```



----
----

#Socio demo prep

 
```{r}

    #Ratio trees df by city 
    resultsList <- getNeighDonCsv(useQc)
    listNeighDone <- resultsList[["listNeighDone"]]
    listNeighDoneCsv <- resultsList[["listNeighDoneCsv"]]

  
    ##Get the df where rows correspond to DAs and that has columns ratioTrees
    dfCity <- getCityDfRatioTrees(useQc)

```

 
```{r}


  listResults <- dataPrepSocioDemoCanopy(dfCity, csd, nS,povertyStr = povertyStr)

  shpDAUnique <- listResults$shpDAUnique
  shpNeigh <- listResults$shpNeigh
  dfByNeigh <- listResults$dfByNeigh
  kmeansResults <- listResults$kmeansResults
```
 
 
---
---



# Density

```{r}


pDensCanopyByneigh <- ggplot(shpDAUnique %>% mutate(neigh=fct_reorder(.f=neigh,.x=weightedRatioTrees,.fun = medNoNa))) + 
  ggridges::geom_density_ridges(aes(x=weightedRatioTrees,y=neigh)) + 
  ylab("") + 
  xlab("Weighted ratio of trees") + 
  ggtitle("Distribution of canopy by neighbourhood") + 
  labs(caption = "Each point represents a dissemination area (DA) within the neighbourhood.\nIf a DA spans multiple canopy polygons,\nthe intersection of DA and canopy gets a weight proportional to its area over the total DA area.")

(pDensCanopyByneigh)

ggsave(here("Figures","canopy",glue("ridgesDensityCanopyByNeigh_{city}.png")),
       plot = pDensCanopyByneigh)

pBoxCanopyByneigh <- ggplot(shpDAUnique %>% mutate(neigh=fct_reorder(.f=neigh,.x=weightedRatioTrees,.fun = medNoNa))) + 
  geom_boxplot(aes(x=weightedRatioTrees,y=neigh)) +
  ylab("") + 
  xlab("Weighted ratio of trees") + 
  ggtitle("Distribution of canopy by neighbourhood") + 
  labs(caption = "Each point represents a dissemination area (DA) within the neighbourhood.\nIf a DA spans multiple canopy polygons,\nthe intersection of DA and canopy gets a weight proportional to its area over the total DA area.")

(pBoxCanopyByneigh)

ggsave(here("Figures","canopy",glue("boxPlotCanopyByNeigh_{city}.png")),
       plot = pBoxCanopyByneigh)

```



##Pop density vs canopy

### Base plots 
```{r}

pDensityVsCanopyNoLab <- ggplot(shpDAUnique %>%  rename(Borough = Arrondissement)) + 
  geom_point(aes(x=popDensityPerHa,y=weightedRatioTrees,col=Borough)) + 
  geom_smooth(aes(x=popDensityPerHa,y=weightedRatioTrees),se = F) + 
  theme_bw() + 
  ggtitle("Population density and proportion of canopy")+
  xlab("Population density per hectare") + 
  ylab("Canopy ratio") + 
  ylim(0,1)

shpLimoilou <- shpDAUnique %>% 
  filter(Arrondissement %in% c("La Cité", "Limoilou")) %>% 
  mutate(neigh=str_replace(string = neigh,pattern = "Vieux-Q.*", 'Vieux-Qc')) %>% 
  rename(Neighbourhood = neigh )
                                      
pDensityVsCanopyLaCiteUnscaled <- ggplot(shpLimoilou ) + 
  geom_point(aes(x=popDensityPerHa,y=weightedRatioTrees,col=Neighbourhood)) + 
  geom_smooth(aes(x=popDensityPerHa,y=weightedRatioTrees), se = F) + 
  theme_bw() + 
  labs(subtitle='La Cite-Limoilou', caption = "Each point represents a dissemination area\n")+
  ggtitle("Population density and proportion of canopy")+
  xlab("Population density per hectare") + 
  ylab("Canopy ratio")

pDensityVsCanopy <- pDensityVsCanopyNoLab + 
  labs(caption = "Each point represents a dissemination area\n") 

pDensityVsCanopyLaCiteScaled <- pDensityVsCanopyLaCiteUnscaled + 
  ylim(0,1)

pBoth <- pDensityVsCanopyNoLab | pDensityVsCanopyLaCiteScaled 

ggsave(here("Figures","canopyVsSociodemo",glue("loessCanopyVsdensity_{city}_wholeCity.png")),
       plot = pDensityVsCanopy,
       height=10,
       width=10)

ggsave(here("Figures","canopyVsSociodemo",glue("loessCanopyVsdensity_{city}_lacitelimoilou.png")),
       plot = pDensityVsCanopyLaCiteUnscaled,
       height=10,
       width=10)

ggsave(here("Figures","canopyVsSociodemo",glue("loessCanopyVsdensity_{city}_both.png")),
       plot = pBoth,
       height=10,
       width=20)

```

###GGhihlight
```{r}


pDensityVsCanopyHighlight <- ggplot(shpDAUnique %>%  rename(Borough = Arrondissement)) + 
  geom_point(aes(x=popDensityPerHa,y=weightedRatioTrees,col=Borough)) + 
  gghighlight(Borough  %in% c("La Cité", "Limoilou") , label_key='Borough', keep_scales=T, use_direct_label=F) + 
  geom_smooth(aes(x=popDensityPerHa,y=weightedRatioTrees),se = F) + 
  theme_bw() + 
  ggtitle("Population density and proportion of canopy")+
  xlab("Population density per hectare") + 
  ylab("Canopy ratio") + 
  ylim(0,1)+
  labs(subtitle = "Quebec City", 
       caption = "Each point represents a dissemination area\nLa Cite-Limoilou highlighted") 

ggsave(here("Figures","canopyVsSociodemo",glue("loessCanopyVsdensity_{city}_wholeCityHighlight.png")),
       plot = pDensityVsCanopyHighlight,
       height=10,
       width=10)

```

### Outliers removed
```{r}


ggplot(shpDAUnique %>% filter(popDensityPerHa<quantile(shpDAUnique$popDensityPerHa,0.95)),
       aes(x=popDensityPerHa,y=weightedRatioTrees,col=Arrondissement)) + 
  geom_point() + 
  geom_smooth()


```
 

##Poverty vs pop density
```{r}

pDensityVsPoverty <- ggplot(shpDAUnique,aes(x= popDensityPerHa  , y= logPoverty )) + 
 geom_point() + 
  geom_smooth(  method = "lm")
 

(pDensityVsPoverty)

ggsave(here("Figures","canopyVsSociodemo",glue("loessDensityVsPoverty_{city}.png")),
       plot = pDensityVsPoverty)


lm( logPovertyScaled~popDensityPerHa ,data=shpDAUniqueNoNa) %>% summary

```

---
---

#Poverty

##Poverty vs canopy
```{r}
 
listCiteNeigh <- c(  "Saint-Roch", "Montcalm","Saint-Sacrement","Saint-Jean-Baptiste", "Saint-Sauveur", "Vieux-Québec/Cap-Blanc/Colline parlementaire")

shpDAUnique %<>% mutate(`La Cite borough` = neigh %in% listCiteNeigh )
 

pPovertyVsCanopy <- ggplot() + 
 geom_point(data= shpDAUnique %>% filter(`La Cite borough`), aes(x= weightedRatioTrees  , y= exp(logPoverty), color=`La Cite borough` ) , alpha=0.8)+ 
  geom_point(data= shpDAUnique %>% filter(`La Cite borough` == F), aes(x= weightedRatioTrees  , y= exp(logPoverty), color=`La Cite borough` ), alpha=0.2 ) + 
 geom_smooth(data= shpDAUnique ,aes(x= weightedRatioTrees  , y= exp(logPoverty)),alpha=0.3) + 
 ggtitle("Relationship between canopy and poverty indicator") + 
 labs(caption = "Each point represents a dissemination area\nOverall smooth trend showed") + 
 xlab("Weighted proportion of trees") + 
 ylab("Poverty indicator")

(pPovertyVsCanopy)

ggsave(here("Figures","canopyVsSociodemo",glue("loessCanopyVsPoverty_{city}.png")),
       plot = pPovertyVsCanopy)

```

##Poverty by neigh
```{r}

dfByNeigh <- shpDAUnique %>% 
  st_set_geometry(NULL) %>% 
  group_by(neigh) %>%
  summarise(medPoverty=median(logPoverty,na.rm=T),
            medRatioTree=median(weightedRatioTrees,na.rm=T))

dfByNeigh %<>% arrange(medRatioTree)
ggplot(shpDAUnique %>% mutate( neigh=factor(neigh,levels=as.character(dfByNeigh$neigh)) ) ) + 
  geom_boxplot(aes(x=neigh, y=weightedRatioTrees, color=neigh  )) +
  theme( axis.text.x = element_text(angle=90, hjust=1) ) + 
  theme(legend.position = "none")
  
dfByNeigh %<>% arrange(medPoverty)

pBoxPoverty <- ggplot(shpDAUnique %>% mutate( neigh=factor(neigh,levels=as.character(dfByNeigh$neigh)) )  ) + 
  geom_boxplot(aes(y=neigh,x=exp(logPoverty) )) +
  theme(legend.position = "none") + 
  ylab("") + 
  xlab("Poverty indicator") + 
  ggtitle("Poverty by neighbourhood") + 
  labs(caption = "Each point represents a dissemination area (DA) within the neighbourhood.")

(pBoxPoverty)

ggsave(here("Figures","canopyVsSociodemo",glue("boxPlotPovertyByneigh_{city}.png")),
       plot = pBoxPoverty)
  

```
 

 
---
---
#Poverty vs canopy

## Poverty vs canopy by Cluster

```{r}

#clusterOriginal represents the ordering of the clusters according to weightedRatioTrees and logPoverty
pScatClusterNoTitle <- ggplot(data=shpDAUnique,aes(x=weightedRatioTrees, y=logPoverty, col = clusterOriginal)) +
  geom_point( size = 1) +
  geom_rug() + 
  geom_smooth(aes(group= clusterOriginal), method="lm", se=F) + 
  theme_minimal() +
  theme(legend.position = "none") + 
  geom_vline(data= kmeansResults$centers, aes(xintercept=weightedRatioTrees),linetype=2) +
   geom_hline(data= kmeansResults$centers, aes_string(yintercept=povertyStr),linetype=2) +
  facet_wrap(~clusterOriginal) + 
  ylab("Log poverty") + 
  xlab("Weighted ratio of trees") 

  

#Use the highlight to showcase a specific group if desired 
if(useHighlight){
  
  clusterIdxLowCanopy <- which.min( kmeansResults$centers[, "weightedRatioTrees"] )
  idxDrop <- ifelse(shpDAUnique$clusterOriginal != clusterIdxLowCanopy, F,T )
  
  pScatClusterNoTitle <- pScatClusterNoTitle + 
  gghighlight(
    idxDrop,
    use_group_by = FALSE,
    unhighlighted_colour = "grey90"
  ) 
}


#Add the title and captions
pScatCluster <- pScatClusterNoTitle + 
  ggtitle("Relationship between poverty and canopy by cluster") + 
  labs(subtitle = "Clusters formed by applying k-means on 35 neighbourhoods\nwith 2 dimensional points consisting of average poverty and average canopy",
       caption = "Group 1: Poorer with less vegetation\nGroup 2: Average poverty and canopy\nGroup 3: Richer with more vegetation")

(pScatCluster)

ggsave( here("Figures","canopyVsSociodemo",glue("scatterPlotByCluster_{city}.png")), 
        pScatCluster)

```


```{r}

pMapClusterNoTitle <- ggplot(shpDAUnique) + 
  geom_sf(aes(fill=cluster), lwd=0.1)+ 
  theme_minimal() + 
  coord_sf(datum=NA) +
  theme(panel.grid.major = element_line(color="transparent"),
        panel.grid = element_blank()) 

  
pMapCluster <- pMapClusterNoTitle + 
  ggtitle(glue("{city} by DA and cluster")) +
  labs(subtitle = "Clusters formed by applying k-means on the {nrow(shpDAUnique)} DAs\nwith 2 dimensional points consisting of log poverty and weighted proportion of canopy",
       caption = "Group 1: Poorer with less vegetation\nGroup 2: Average poverty and canopy\nGroup 3: Richer with more vegetation")
  
(pMapCluster)
  
ggsave( here("Figures","canopyVsSociodemo","maps", glue("mapByCluster_{city}.png")), 
        pMapCluster)


```

### Concat both graphs
```{r}


pBoth <- pMapClusterNoTitle + 
  pScatClusterNoTitle + 
  patchwork::plot_annotation(
    title = glue("{city} by neighbourhood and cluster"),
    subtitle  = glue("Clusters formed by applying k-means on the {nrow(shpDAUnique)} DAs\nwith 2 dimensional points consisting of log poverty and weighted proportion of canopy"),
    caption = "Group 1: Poorer with less vegetation\nGroup 2: Average poverty and canopy\nGroup 3: Richer with more vegetation\nDotted lines show cluster centroid" ) +
  plot_layout(ncol = 2, widths = c(1,2))

(pBoth)

  
ggsave( here("Figures","canopyVsSociodemo","maps", glue("bothMapAndScatterByCluster_{city}.png")), 
        pBoth,
        width = 10)
```

 
 
 
 
## Plot the relationship between canopy and poverty by neighbourhood in grid graph
```{r}

useLogPoverty <- T
respStr <- ifelse(useLogPoverty, "logPoverty", "logPovertyScaled") 

dfLmByNeigh <- getDfLmByNeigh(shpDAUnique,
                              resp=respStr)

```
 
 
```{r}
 
p <- ggplot(shpDAUnique %>% mutate(neigh=factor(neigh,levels=dfLmByNeigh$neigh))  %>% mutate( str_replace( neigh, ".*Vieux-Québec.*" ,"Vieux-Québec" )) ) + 
  geom_point(aes(x=weightedRatioTrees,y=logPoverty, color=neigh  )) + 
  geom_smooth(aes(x=weightedRatioTrees,y=logPoverty), method = "lm") + 
  facet_wrap(~neigh) + 
  theme(legend.position = "none")
  
(p)

 respStrCap <- respStr %>% Hmisc::capitalize()
 ggsave(here("Figures","canopyVsSociodemo",glue("gridPlot{respStrCap}VsCanopy_{city}.png")),
        p,
        width = 10,
        height = 10)
```



##Poverty vs canopy - facet by density quantiles

```{r}

numGroups <- 10
shpDAUnique %<>% mutate( densityGroup = cut_number(popDensityPerHa, n=numGroups) ) 

dfByGroup <- shpDAUnique %>% 
  st_set_geometry(NULL) %>% 
  group_by(densityGroup) %>% 
  summarise(averageDensity=mean(popDensityPerHa),
            averageWeightedRatioTrees=mean(weightedRatioTrees),
            averagePoverty=mean(logPoverty))

dfByGroup %>% head
```



```{r}

#clusterOriginal represents the ordering of the clusters according to weightedRatioTrees and logPoverty
pScatClusterNoTitle <- ggplot(data=shpDAUnique,aes(x=weightedRatioTrees, y=logPoverty, col = densityGroup)) +
  geom_point( size = 1) +
  geom_rug() + 
  geom_smooth(aes(group= densityGroup), method="lm", se=F) + 
  theme_minimal() +
  theme(legend.position = "none") + 
  geom_vline(data=dfByGroup, aes(xintercept=averageWeightedRatioTrees),linetype=2) +
   geom_hline(data= dfByGroup, aes(yintercept=averagePoverty),linetype=2) +
  facet_wrap(~densityGroup) + 
  ylab("Log poverty") + 
  xlab("Weighted ratio of trees") 

   


#Add the title and captions
pScatCluster <- pScatClusterNoTitle + 
  ggtitle("Relationship between poverty and canopy by cluster") + 
  labs(subtitle = "Clusters formed by taking quantiles of the population density")

(pScatCluster)

ggsave( here("Figures","canopyVsSociodemo",glue("scatterPlotByDensityGroup_{numGroups}_{city}.png")), 
        pScatCluster)

```


```{r}

pMapClusterNoTitle <- ggplot(shpDAUnique) + 
  geom_sf(aes(fill=densityGroup), lwd=0.1)+ 
  theme_minimal() + 
  coord_sf(datum=NA) +
  theme(panel.grid.major = element_line(color="transparent"),
        panel.grid = element_blank()) 

  
pMapCluster <- pMapClusterNoTitle + 
  ggtitle(glue("{city} by DA and density quantile")) +
  labs(subtitle = "Clusters formed by taking quantiles of the population density")
  
(pMapCluster)
  
ggsave( here("Figures","canopyVsSociodemo","maps", glue("mapByDensityGroup_{numGroups}_{city}.png")), 
        pMapCluster)


```

### Concat both graphs
```{r}


pBoth <- pMapClusterNoTitle + 
  pScatClusterNoTitle + 
  patchwork::plot_annotation(
    title = glue("{city} by neighbourhood and cluster"),
    subtitle = "Clusters formed by taking quantiles of the population density") + 
  plot_layout(ncol = 2, widths = c(1,2))

(pBoth)

  
ggsave( here("Figures","canopyVsSociodemo",glue("bothMapAndScatterByDensityGroup_{numGroups}_{city}.png")), 
        pBoth,
        width = 10)
```
