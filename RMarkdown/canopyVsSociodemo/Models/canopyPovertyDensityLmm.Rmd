---
title: "Untitled"
author: "Charles"
date: "November 25, 2019"
output: html_document
---
 

#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
library(miceadds)
library(glue)
library(sf)
 
library(gstat)


library(lme4)
library(lmerTest)
library(mgcv)


library(ggplot2)
library(ggrepel)
library(gghighlight)
library(cowplot)

library(patchwork)
```

#Parameters
```{r}

options(na.action = "na.exclude")

set.seed(123)

useQc <- T

csd <- ifelse(useQc,  "2423027",  "2466023")
city <- ifelse(useQc, "Quebec", "Montreal")

Census2SfSp:::setCancensusOptions()


  if(useQc){
    nS <- neighShp::neighShpQc$new( neighPath = here("Data","GeoData", "QcNeighbourhoods") )
  }else{
    nS <- neighShp::neighShpMtl$new(  )
  }

idVar <-  nS$getID()
  


```



----
----

#Socio demo prep

 
```{r}

    #Ratio trees df by city 
    resultsList <- getNeighDonCsv(useQc)
    listNeighDone <- resultsList[["listNeighDone"]]
    listNeighDoneCsv <- resultsList[["listNeighDoneCsv"]]

  
    ##Get the df where rows correspond to DAs and that has columns ratioTrees
    dfCity <- getCityDfRatioTrees(useQc)

```

 
```{r}


  listResults <- dataPrepSocioDemoCanopy(dfCity, csd, nS)

  shpDAUnique <- listResults$shpDAUnique
  shpNeigh <- listResults$shpNeigh
  dfByNeigh <- listResults$dfByNeigh
  kmeansResults <- listResults$kmeansResults
  
  shpDAUniqueNoNa <- shpDAUnique %>%
  filter_at( vars(c("weightedRatioTrees","logPovertyScaled")), all_vars(!is.na(.)) )

```
 
 

----
----

#Poverty as fct of canopy


##Basic lm
```{r}

lmFit <- lm( logPovertyScaled ~ weightedRatioTrees  , 
                    data=shpDAUnique)

lmFit %>% summary

dfLmOverallTrans <- lmFit %>% 
  broom::tidy() %>% 
  select(term,estimate) 

dfLmOverall <- dfLmOverallTrans %>% 
  select(estimate) %>% 
  t() %>% 
  as.data.frame() %>% 
  set_names(nm = dfLmOverallTrans %>% pull(term))
 
dfLmOverall

par(mfrow=c(2,2))
plot(lmFit)

pLmResid <- plotResidByneighBox(mdlFit = lmFit, 
                regType =  "Gaussian", 
                resp =  "logPovertyScaled", 
                covarStr =  "weightedRatioTrees",
                nameStr = "lmFit")

 
(pLmResid)

```

##Mixed linear models


###Simple random intercept by neigh
```{r}

mixedLmFit <- lmer( logPovertyScaled ~ weightedRatioTrees + (1|neigh), 
                    data=shpDAUnique,
                    REML = F)

mixedLmFit %>% summary

dfLmFit <- coef(mixedLmFit)[["neigh"]]
dfLmFit$neigh <- rownames(dfLmFit)


pLmerResid <- plotResidByneighBox(mdlFit = mixedLmFit, 
                 regType = "Mixed Gaussian", 
                 resp = "\nlogPovertyScaled",
                  covarStr = "weightedRatioTrees + (1|neigh)",
                 nameStr = "mixedLmFit")


(pLmerResid)
 

confint(mixedLmFit)
 
png( here("Figures","canopyVsSociodemo","models", "dotPlot", glue("lmer1DotPlot_{city}.png")))
dotplot(ranef(mixedLmFit,condVar=T))
dev.off()

dfEffectsIntercept <- coef(mixedLmFit)$neigh
dfEffectsIntercept$neigh <- rownames(dfEffectsIntercept)

```

 

###Random slope + intercept by neigh
```{r}

mixedLmFit2 <- lmer( logPovertyScaled ~ weightedRatioTrees + (1+weightedRatioTrees|neigh), 
                    data=shpDAUnique,
                    REML = F)

mixedLmFit2 %>% summary
 

#Hum, many conf intervals for the weightedRatioTrees contain 0
#The variance for weightedRatioTrees is higher than for (Intercept)  and both are higher than the residuals
png( here("Figures","canopyVsSociodemo","models", "dotPlot", glue("lmer2DotPlot_{city}.png")))
dotplot(ranef(mixedLmFit2,condVar=T))
dev.off()

confint(mixedLmFit2)


dfEffectsInterceptSlope <- coef(mixedLmFit2)$neigh
dfEffectsInterceptSlope$neigh <- rownames(dfEffectsInterceptSlope)
```

#### Plot the residuals with boxplot by cluster
```{r}


pLmerResid2 <- plotResidByneighBox(mixedLmFit2, 
                 "Mixed Gaussian", 
                 resp = "\nLog poverty scaled" ,
                 "weightedRatioTrees + (1+weightedRatioTrees|neigh)",
                 nameStr = "lmer2",
                 city=city)

(pLmerResid2)
 


```

#### Plot the residuals with scatter by cluster for the lmer2
```{r}


plotResidByneighScatter(mdlFit = mixedLmFit2,
                        regType = "Mixed model" ,
                        resp = "\nLog poverty scaled" ,
                        covarStr ="weightedRatioTrees + (1+weightedRatioTrees|neigh)",
                        nameStr = "lmer2",
                        city = city 
                        )

```


### Random slope + intercept by neigh + add pop density as fixed effect
```{r}

#The new fixed effect is also significant
mixedLmFit3 <- lmer( logPovertyScaled ~ weightedRatioTrees + popDensityPerHa +  (1+weightedRatioTrees|neigh), 
                    data=shpDAUnique )

mixedLmFit3 %>% summary


confint(mixedLmFit3)


png( here("Figures","canopyVsSociodemo","models", "dotPlot", glue("lmer3DotPlot_{city}.png")))
dotplot(ranef(mixedLmFit3,condVar=T))
dev.off()

```
 

###Random slope + intercept by neigh + spatial correlation within neigh



```{r}
 

mixedLmFit4 <- nlme::lme( logPovertyScaled ~ weightedRatioTrees , 
                            random =  ~1+weightedRatioTrees| neigh  ,
                          correlation=corExp( form=~lng+lat), 
                          data=shpDAUniqueNoNa)

mixedLmFit4 %>% summary
 
pLmerResid4 <- plotResidByneighBox(mixedLmFit4, 
                 "Mixed Gaussian", 
                 "\nlogPovertyScaled", 
                 " weightedRatioTrees + (1+weightedRatioTrees|neigh)\nwith corExp( form=~lng+lat)",
                 nameStr = "mixedLmFit4",
                 shpCpy=shpDAUniqueNoNa)

(pLmerResid4)
 


#Hum, many conf intervals for the weightedRatioTrees contain 0
#The variance for weightedRatioTrees is higher than for (Intercept)  and both are higher than the residuals
png( here("Figures","canopyVsSociodemo","models", "dotPlot",glue("lmer4DotPlot_{city}.png")))
dotplot(ranef(mixedLmFit4,condVar=T))
dev.off()

 

dfEffectsInterceptSlope4 <- coef(mixedLmFit4)
dfEffectsInterceptSlope4$neigh <- rownames(dfEffectsInterceptSlope)
```
 
 
 

###Random slope + intercept by neigh + canopy quantile
```{r}

shpDAUnique %<>% mutate( neighRatio = glue("{neigh}_{as.integer(ratioTreeQuantClass)}") %>% as.factor() )

mixedLmFit5 <- lmer( logPovertyScaled ~ weightedRatioTrees + (1+weightedRatioTrees|neighRatio), 
                    data=shpDAUnique,
                    REML = F)

mixedLmFit5 %>% summary
 

#Hum, many conf intervals for the weightedRatioTrees contain 0
#The variance for weightedRatioTrees is higher than for (Intercept)  and both are higher than the residuals
png( here("Figures","canopyVsSociodemo","models", "dotPlot",glue("lmer5DotPlot_{city}.png")),width = 1000, height = 1000)
dotplot(ranef(mixedLmFit5,condVar=T))
dev.off()

#confint(mixedLmFit5)


dfEffectsInterceptSlope <- coef(mixedLmFit5)$neigh
dfEffectsInterceptSlope$neigh <- rownames(dfEffectsInterceptSlope)
```



###Random slope + intercept by (neigh:densityQuantile)
```{r}


shpDAUnique %<>% mutate( popDensityPerHaQuantile = cut_number(popDensityPerHa,n=4))
shpDAUnique %<>% mutate( neighPopQuant = glue("{neigh}_{as.integer(popDensityPerHaQuantile)}") %>% as.factor() )

mixedLmFit6<- lmer( logPovertyScaled ~ weightedRatioTrees + (1+weightedRatioTrees|neighPopQuant), 
                    data=shpDAUnique,
                    REML = F)

mixedLmFit6 %>% summary
 

#Hum, many conf intervals for the weightedRatioTrees contain 0
#The variance for weightedRatioTrees is higher than for (Intercept)  and both are higher than the residuals
png( here("Figures","canopyVsSociodemo","models", "dotPlot",glue("lmer6DotPlot_{city}.png")),width = 1000, height = 1000)
dotplot(ranef(mixedLmFit6,condVar=T))
dev.off()

 

dfEffectsInterceptSlope <- coef(mixedLmFit6)$neigh
dfEffectsInterceptSlope$neigh <- rownames(dfEffectsInterceptSlope)

#Smaller values of popDensityPerHaQuantile are associated with lower densities
#ggplot(shpDAUnique) + geom_sf(aes(fill=(as.integer(popDensityPerHaQuantile))),lwd=0)

#Sort by the average intercept per neigh 
dfRanef <- dfEffectsInterceptSlope %>% 
  rownames_to_column %>% 
  as_tibble() %>%  
  separate(col=rowname,into = c("neigh", "popDensityPerHaQuantile"),sep = "_" ) 

dfRanef %<>% mutate(neigh= fct_reorder( .f = neigh, .x = `(Intercept)` ,.fun = meanNoNa  ) )

dfRanef %<>% arrange(neigh)

#Hum, ok the slopes do not qualitatively seem radically different for different density quantiles
#The main effects have smaller std errors, but since there are less obs per group the variance of the random effects is larger than mixed models with only neigh as ranef

ggplot(dfRanef  ) + 
  geom_abline(aes(slope=weightedRatioTrees, intercept = `(Intercept)`, col=popDensityPerHaQuantile)) + 
  geom_point(data=shpDAUnique %>% mutate(popDensityPerHaQuantile=as.factor(as.integer(popDensityPerHaQuantile) )), aes(x=weightedRatioTrees, y=logPovertyScaled ,col=popDensityPerHaQuantile)) + 
  facet_wrap(~neigh)
  

```


###Inspect random effects
```{r}

dfRE <- ranef(mixedLmFit5,condVar=T) %>% as_tibble() %>% dplyr::select(-grpvar)
dfRE %<>% separate(grp,into = c("neigh","canopyQuantile"), sep="_" )
dfRE %<>% arrange(neigh,canopyQuantile)
dfRE %<>% mutate(neigh=factor(neigh,levels=as.character(unique(neigh))))
dfRESlope <- dfRE %>% filter(term == "weightedRatioTrees" )

#Hum, not really sure how we can interpret this graph, but the effect definitely seems to change according to wether the area has many trees or not and this effect is not uniform thrughout neighburhods
ggplot(dfRESlope)+ 
  geom_line(aes(x=canopyQuantile,y=condval, group=neigh)) + 
  geom_point(aes(x=canopyQuantile,y=condval) ) + 
  geom_hline(aes(yintercept=0), linetype="dashed") + 
  facet_wrap(~ neigh) + 
  ggtitle("Conditional mode for slope")

 
```


#### Plot the residuals with boxplot by cluster
```{r}

 
plotResidByneighBox(mixedLmFit5, 
                 "Mixed Gaussian", 
                 resp = "\nLog poverty scaled" ,
                 "weightedRatioTrees +  (1+weightedRatioTrees|neigh:ratioTreeQuantClass)",
                 nameStr = "lmer5",
                 city=city)

 


```

#### Plot the residuals with scatter by cluster for the lmer2
```{r}


plotResidByneighScatter(mdlFit = mixedLmFit5,
                        regType = "Mixed model" ,
                        resp = "\nLog poverty scaled" ,
                        covarStr ="weightedRatioTrees +  (1+weightedRatioTrees|neigh:ratioTreeQuantClass)",
                        nameStr = "lmer5",
                        city = city 
                        )

```


##Anova to test for relevance of added fixed effects  
```{r}

#Added complexity seems worth while 
anova(mixedLmFit, mixedLmFit2,mixedLmFit3,mixedLmFit4,mixedLmFit5)
 
``` 


---
---

#Compare results of lm by neigh vs. random+fixed effects
```{r, message=F}


dfLmByNeigh <- getDfLmByGroup( shpDAUnique , 
                                       slopeStr= "weightedRatioTrees",
                                       resp="logPovertyScaled",
                                varFilter="neigh"
                      )

dfLmByNeigh %<>% rename( interByNeigh=inter, slopeByNeigh=slope)
 
dfLmByNeighIntercept <- dfLmByNeigh
dfLmByNeighInterceptSlope <- dfLmByNeigh


dfLmByNeighIntercept %<>% left_join(dfEffectsIntercept)
dfLmByNeighInterceptSlope %<>% left_join(dfEffectsInterceptSlope)

dfLmByNeighIntercept %>% head
dfLmByNeighInterceptSlope %>% head
```


## Plot the 'contraction/reduction' effect on 'parameters' from lmer vs fit by factor level

### 1st mixed model - only slope
```{r}

#Row stack the different models (lmbyneigh,lmoverall,lmer) for the points
#Use the column stacked dfLmByNeigh for segments
df1 <- dfLmByNeighIntercept %>% 
  select(neigh, interByNeigh, slopeByNeigh) %>% 
  rename(inter=interByNeigh, slope=slopeByNeigh)
df1$id <- rep("byNeigh", nrow(df1))
df1$size <-  rep(1, nrow(df1))

df2 <- dfLmByNeighIntercept %>% 
  select(neigh, `(Intercept)`, weightedRatioTrees) %>% 
  rename(inter=`(Intercept)`, slope=weightedRatioTrees)
df2$id <- rep("lmer2", nrow(df2))
df2$size <-  rep(1, nrow(df2))

df3 <- dfLmOverall%>% 
  rename(inter=`(Intercept)`, slope=weightedRatioTrees)
df3$id <- rep("lmOverall", nrow(df3))
df3$size <-  rep(4, nrow(df3))

dfBind <- bind_rows(df1,df2,df3)



pRegularizationLmer1 <-ggplot( ) + 
  geom_point(data=dfBind, aes(x=inter,y=slope,color=id,size=size)) + 
  geom_segment(data=dfLmByNeighIntercept, 
               aes(x=interByNeigh, xend=`(Intercept)`,
                   y=slopeByNeigh, yend=weightedRatioTrees),
                arrow = arrow(length = unit(0.03, "npc"))) +
  guides(size = FALSE) + 
  #ggtitle("Comparing mixed effects and linear model by neighbourhood") + 
  labs(subtitle = "Mixed model with only the random intercept")
 
(pRegularizationLmer1)
```

### 2nd mixed model
```{r}

#Row stack the different models (lmbyneigh,lmoverall,lmer) for the points
#Use the column stacked dfLmByNeigh for segments
df1 <- dfLmByNeighInterceptSlope %>% 
  select(neigh, interByNeigh, slopeByNeigh) %>% 
  rename(inter=interByNeigh, slope=slopeByNeigh)
df1$id <- rep("byNeigh", nrow(df1))
df1$size <-  rep(1, nrow(df1))

df2 <- dfLmByNeighInterceptSlope %>% 
  select(neigh, `(Intercept)`, weightedRatioTrees) %>% 
  rename(inter=`(Intercept)`, slope=weightedRatioTrees)
df2$id <- rep("lmer2", nrow(df2))
df2$size <-  rep(1, nrow(df2))

df3 <- dfLmOverall%>% 
  rename(inter=`(Intercept)`, slope=weightedRatioTrees)
df3$id <- rep("lmOverall", nrow(df3))
df3$size <-  rep(4, nrow(df3))

dfBind <- bind_rows(df1,df2,df3)



pRegularizationLmer2 <- ggplot( ) + 
  geom_point(data=dfBind, aes(x=inter,y=slope,color=id,size=size)) + 
  geom_segment(data=dfLmByNeighInterceptSlope, 
               aes(x=interByNeigh, xend=`(Intercept)`,
                   y=slopeByNeigh, yend=weightedRatioTrees),
                arrow = arrow(length = unit(0.03, "npc"))) +
  guides(size = FALSE) + 
  #ggtitle("Comparing mixed effects and linear model by grouping factor") + 
  labs(subtitle = "Mixed model with random slope and intercept")
 
(pRegularizationLmer2)

```

```{r}

pBothRegularization <- pRegularizationLmer1 + pRegularizationLmer2 + 
  plot_annotation(title = "Comparing mixed effects and linear model by neighbourhood" )

(pBothRegularization)

 ggsave(here("Figures","canopyVsSociodemo","models", "regularization" , glue("regularizatioMixedModels_{city}.png")),
        pBothRegularization)

```

---
---
 

 

#Inspect residuals for poverty vs ratioOfTrees

```{r}

#Watch out, seems some neigh were not used by lmer, but are present in the df:
#probably due to missing values
shpDAUnique$errorLmOverall <- predict(lmFit, newdata = shpDAUnique) - shpDAUnique$logPovertyScaled


shpDAUnique$errorLmer2 <- predict(mixedLmFit2, newdata = shpDAUnique, allow.new.levels = T) - shpDAUnique$logPovertyScaled

shpDAUnique$errorRnd <- sample(shpDAUnique$errorLmer2,  size=nrow(shpDAUnique), replace = T)

# Don't include these models
# shpDAUnique$errorLmer <- predict(mixedLmFit, newdata = shpDAUnique, allow.new.levels = T) - shpDAUnique$logPovertyScaled

shpDAUnique$errorLmer3 <- predict(mixedLmFit3, newdata = shpDAUnique, allow.new.levels = T)  - shpDAUnique$logPovertyScaled

 shpDAUnique$errorLmer4 <- predict(mixedLmFit4, newdata = shpDAUnique, allow.new.levels = T)  - shpDAUnique$logPovertyScaled

  shpDAUnique$errorGls5 <- predict(glsFit5, newdata = shpDAUnique, allow.new.levels = T)  - shpDAUnique$logPovertyScaled
 
allErrors <- shpDAUnique %>% st_set_geometry(NULL) %>% select(contains("error")) %>% melt %>% pull(value)
allErrors <- allErrors[!is.na(allErrors)]



shpMelted <- shpDAUnique %>% 
  st_set_geometry(NULL)  %>% 
  select(contains("error"), GeoUID,neigh, cluster, weightedRatioTrees)  %>%
  melt(id.vars=c("GeoUID","neigh", "cluster", "weightedRatioTrees"))

shpMelted  %<>% left_join(shpDAUnique %>% select(GeoUID) )
shpMelted %<>% st_set_geometry(shpMelted$geometry)
```

#Quick inspection
```{r}

shpDAUnique %>% 
  top_n(abs(errorLmer2),n=10) %>% 
  select(neigh, cluster, errorLmer2 ,weightedRatioTrees ,logPovertyScaled )

```

#Check spatial correlation - sf
```{r}

#All models show significant spatial correlation
#Missing some variables if we want better prediction and more robust inference

#Good idea to keep the missing values in grey, otherwise, they could be mistaken for 0 errors
pMapResiduals <- ggplot(data=shpMelted, aes(fill=value)) +
 geom_sf( lwd=0) + 
  facet_wrap(~variable) + 
  theme_minimal()+
  coord_sf(datum=NA) +
  theme(panel.grid.major = element_line(color="transparent"),
        panel.grid = element_blank()) +
  scale_fill_gradient2(low="blue",mid="white", high="red",space="Lab")
 
(pMapResiduals)

 ggsave(here("Figures","canopyVsSociodemo","models",  "maps", glue("mapMixedModelsResiduals_{city}.png")),
        pMapResiduals)

```

## Check correlation within neighbourhood
```{r}


pBoxPlotResiduals <- ggplot(data=shpMelted, aes(x=neigh,y=value, col=cluster)) +
 geom_boxplot(  ) + 
  geom_hline(aes(yintercept=0), linetype=2) + 
  facet_wrap(variable~.,nrow = 1) + 
  theme_minimal()+
  coord_flip() + 
  theme(axis.text.x = element_text(angle=90))  

 (pBoxPlotResiduals)

 ggsave(here("Figures","canopyVsSociodemo","models", "residuals", "byNeigh", glue("boxPlotMixedModelsResidualsByNeigh_{city}.png")),
        pBoxPlotResiduals)

```

##By cluster
```{r}

pBoxPlotByClustResiduals <- ggplot(data=shpMelted %>% mutate(str_replace( neigh, ".*Vieux-Québec.*" ,"Vieux-Québec") ) , aes(x=cluster,y=value, col=cluster)) +
 geom_boxplot(  ) + 
  geom_hline(aes(yintercept=0), linetype=2) + 
  facet_wrap(~variable) + 
  theme_minimal()+
  theme(axis.text.x = element_text(angle=90))  +
  ggtitle("Mixed models residuals by cluster")

(pBoxPlotByClustResiduals)

 ggsave(here("Figures","canopyVsSociodemo","models", "residuals", "byClust",glue("boxPlotMixedModelsResidualsByCluster_{city}.png")),
        pBoxPlotByClustResiduals)

```

##Plot fitted vs residuals by neigh
```{r}

pFacetScatterXVsPredict <- ggplot(data=shpMelted %>% mutate( str_replace( neigh, ".*Vieux-Québec.*" ,"Vieux-Québec" )), aes(x=weightedRatioTrees,y=value, col=variable)) + 
geom_point( aes(x=weightedRatioTrees, y=value) ,alpha=0.6, shape=21) + 
  facet_wrap(~neigh)

(pFacetScatterXVsPredict)

 ggsave(here("Figures","canopyVsSociodemo","models", "residuals", "byNeigh", glue("scatterPlotXVsPredByNeigh_{city}.png")),
        pFacetScatterXVsPredict,
        width = 10,
        height = 10)

```

## Moran's I
```{r}
 


computeMoranI(shpDAUnique, "errorLmOverall")
computeMoranI(shpDAUnique, "errorLmer2")     #least correlated residuals
computeMoranI(shpDAUnique, "errorRnd") 


```

## Variogram 
```{r}

v <- variogram( errorLmer2~errorLmer2,
           shpDAUnique %>% filter_all(all_vars(!is.na(.) )) %>% st_zm %>%  sf::as_Spatial())


pAutoCorr <- ggplot(v,aes(x=dist,y=gamma)) + 
  geom_point(aes(size=np)) + 
  ggtitle("Presence of spatial correlation")+
  labs(subtitle = "Mixed model residuals with random slope and intercept")

(pAutoCorr)

ggsave(here("Figures","canopyVsSociodemo", "models", "maps" , glue("variogramLmer2_{city}.png")),
       plot=pAutoCorr,
       height=7,
       width=10)

```
 


## Inspect errors with continuous fct of lng lat


```{r}

shpDAUnique %<>% SfSpHelpers::getCentroids()

gammFit <- gamm(formula =  logPovertyScaled ~ weightedRatioTrees +  s(lng,lat), 
           random =  list(neigh=~weightedRatioTrees),
           data=shpDAUnique )
 
png(here("Figures","canopyVsSociodemo", "models", "surface3DSmoothErrorsGamm.png"))
vis.gam(gammFit$gam,view = c("lng","lat"),type = "response", phi=45,color = "cm")
dev.off()

plot(gammFit$gam)

gammFit$lme %>% summary

```



## Variogram by neigh
```{r}


dfVar <- data.frame( err=c("errorLmer2", "errorLmer3", "errorLmer4", "errorGls5"),
                     titles = c("Mixed model","Mixed model", "Mixed model", "GLS"),
                     nameStr = c("lmer2","lmer3", "lmer4", "gls5")
                     )

dfVariograms <- map_df(1:nrow(dfVar), 
                       ~computeVariogramByNeigh(shpDAUnique ,
                                         dfVar$err[[.x]] %>% as.character(), #watch out for factors
                                         dfVar$title[[.x]],
                                         dfVar$nameStr[[.x]] , 
                                         city="Quebec")
)


```


<!-- ##Spatial correlation by neigh -->
<!-- ```{r} -->

<!-- #Raw values  -->
<!-- corToIns <- corExp(form =  ~ weightedRatioTrees |neigh , nugget=T,value = 0.8)  -->
<!-- corSpatialRaw <- nlme::Initialize(corToIns, data=shpDAUnique) -->
<!-- listCorRaw <- corMatrix(corSpatialRaw) -->


<!-- dfAllCorrMatRaw <- data.frame() -->
<!-- for(k in 1:length(listCorRaw)){ -->
<!--   df <- listCorRaw[[k]] %>% melt() -->
<!--   df$neigh <- names(listCorRaw)[[k]] -->
<!--   dfAllCorrMatRaw <- bind_rows(dfAllCorrMatRaw,df) -->
<!-- } -->

<!-- pRaw <- ggplot(dfAllCorrMatRaw) +  -->
<!--   geom_tile(aes(x=Var1,y=Var2,fill=value)) +  -->
<!--   facet_wrap(~neigh) -->


<!-- #Residuals -->
<!-- shpDAUnique$errorLmer2 <- predict(mixedLmFit2, newdata = shpDAUnique, allow.new.levels = T) - shpDAUnique$logPovertyScaled -->

<!-- corToIns <- corExp(form =  ~ errorLmer2 |neigh , nugget=T,value = 0.8)  -->
<!-- corSpatialLmer2<- nlme::Initialize(corToIns, data=shpDAUnique) -->
<!-- listCorLmer2<- corMatrix(corSpatialLmer2) -->


<!-- dfAllCorrMatLmer2<- data.frame() -->
<!-- for(k in 1:length(listCorLmer2)){ -->
<!--   df <- listCorLmer2[[k]] %>% melt() -->
<!--   df$neigh <- names(listCorLmer2)[[k]] -->
<!--   dfAllCorrMatLmer2 <- bind_rows(dfAllCorrMatLmer2,df) -->
<!-- } -->

<!-- pLmer2 <- ggplot(dfAllCorrMatLmer2) +  -->
<!--   geom_tile(aes(x=Var1,y=Var2,fill=value)) +  -->
<!--   facet_wrap(~neigh) -->

<!-- library(patchwork) -->
<!-- pRaw + pLmer2 -->


<!-- ``` -->

 
 